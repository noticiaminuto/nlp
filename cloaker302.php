<?php
$src = isset($_GET["src"]) ? $_GET["src"] : '';
$user_agent = $_SERVER['HTTP_USER_AGENT'].'';
$fbclid = isset($_GET["fbclid"]) ? $_GET["fbclid"] : null;

// If the parameter verbose is passed, verbose info will be output.
// You can set any value to verbose, only its presence matters.
$verbose = isset($_GET["verbose"]) ? true : null;

$country_code = NULL;
if ($filter_by_country) {
    //$country_code = ip_info(NULL, "countrycode");
    
    // CF_IPCOUNTRY is the country identified by the cloudflare service. This is
    // faster than using geolocation.
    $cf_country_code = isset($_SERVER['HTTP_CF_IPCOUNTRY'])
        ? $_SERVER['HTTP_CF_IPCOUNTRY'] : 'BR';
    $country_code = $cf_country_code;
}

$referer = null;
if ($filter_by_referer !== false) {
    $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
}

/* In order to make testing easier, I created a "debug mode". In order to use it
you should pass the parameter token='2b36735170794e33899842464502c0ee' on the
request and then all parameters you want to use. In this case, the returned
html will simply be 'SAFE' or 'UNSAFE' depending on the parameters you pass.

If you don't send the debug token, then the retrned html will be the contents
of the html files pointed by $SAFE_SITE or $UNSAFE_SITE.
 */
$token = isset($_GET['token']) ? $_GET['token'] : '';

$isAllowed = FALSE;

$headers = strtolower(get_request_headers());

if (!debug($token)) {
    $reason = '';
    $isAllowed = isAllowed($src, $reason, $filter_by_src, $src_whitelist, $src_blacklist,
        $user_agent, $filter_by_device, $device_whitelist, $device_blacklist,
        $country_code, $filter_by_country, $country_whitelist, $country_blacklist,
        $fbclid, $filter_by_fbclid,
        $referer, $filter_by_referer, $referer_whitelist, $referer_blacklist,
        $referer_safe_if_absent, $referer_safe_if_first, $headers);

    if ($save_to_database) {
        /* in order to create the database and tables, uncomment the following
        line and run it once on the server. then you can comment it again. */
        //create_database();
        
        $ip = get_ip();
        $device = userDevice($user_agent);
        $robot = isCrawler($user_agent);
        $request = get_request();
        $headers = get_request_headers();
        save_to_database($site_identification, $ip, $src, $country_code,
        $user_agent, $device, $robot ? $robot : null,
        $page_seen=$isAllowed ? 'unsafe' : 'safe', $request, $headers, $reason);
    }

    $queryString = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
    // If the user set fbclid_replace to a different value than null we replace
    // this url param.
    if ($fbclid != NULL) {
        if ($filter_by_fbclid) {
            if ($fbclid_replace != NULL) {
                $queryString = str_replace(
                    'fbclid='.$_GET['fbclid'],
                    'fbclid='.$fbclid_replace,
                    $queryString
                );
            }
        }
    }

    

    

    if($isAllowed) {
        header('Location: '.$UNSAFE_SITE_REDIRECT_URL.$queryString,
            true, 302);
        exit;
    } else {
        if ($SAFE_SITE_REDIRECT_URL != null || $SAFE_SITE_REDIRECT_URL != "") {
            header('Location: '.$SAFE_SITE_REDIRECT_URL.$queryString,
                true, 302);
            exit;
        }
    }
}

function verbose($message) {
    /* Echoes to the output a verbose message only if the global variable
    $verbose is true */
    global $verbose;

    if ($verbose) {
        echo $message;
    }

}

function debug($token='') {
    /* Runs the script in debug mode.
    In this mode, all parameters should be sent on the request. This allows
    testing all the methods which determine if the user can see the unsafe page.

    The URL parameters should be:
    src, filter_by_src, src_whitelist, src_blacklist, user_agent,
    filter_by_device, device_whitelist, device_blacklist, country_code,
    filter_by_country, country_whitelist, country_blacklist,
    $fbclid, $filter_by_fbclid.

    You can use 0 and 1 instead of false and true for the parameters which
    accept booleans. */
    if($token=='2b36735170794e33899842464502c0ee') {
        $src = isset($_GET['src']) ? $_GET['src'] : '';
        $filter_by_src = isset($_GET['filter_by_src']) ? $_GET['filter_by_src'] : false;
        $src_whitelist = isset($_GET['src_whitelist']) ? $_GET['src_whitelist'] : '';
        $src_blacklist = isset($_GET['src_blacklist']) ? $_GET['src_blacklist'] : '';
        $user_agent = isset($_GET['user_agent']) ? $_GET['user_agent'] : '';
        $filter_by_device = isset($_GET['filter_by_device']) ? $_GET['filter_by_device'] : false;
        $device_whitelist = isset($_GET['device_whitelist']) ? $_GET['device_whitelist'] : '';
        $device_blacklist = isset($_GET['device_blacklist']) ? $_GET['device_blacklist'] : '';
        $country_code = isset($_GET['country_code']) ? $_GET['country_code'] : null;
        $filter_by_country = isset($_GET['filter_by_country']) ? $_GET['filter_by_country'] : false;
        $country_whitelist = isset($_GET['country_whitelist']) ? $_GET['country_whitelist'] : '';
        $country_blacklist = isset($_GET['country_blacklist']) ? $_GET['country_blacklist'] : '';
        $fbclid = isset($_GET['fbclid']) ? $_GET['fbclid'] : null;
        $filter_by_fbclid = isset($_GET['filter_by_fbclid']) ? $_GET['filter_by_fbclid'] : false;
        $referer = isset($_GET['referer']) ? $_GET['referer'] : null;
        $filter_by_referer = isset($_GET['filter_by_referer']) ? $_GET['filter_by_referer'] : false;
        $referer_whitelist = isset($_GET['referer_whitelist']) ? $_GET['referer_whitelist'] : '';
        $referer_blacklist = isset($_GET['referer_blacklist']) ? $_GET['referer_blacklist'] : '';
        $referer_safe_if_absent = isset($_GET['referer_safe_if_absent']) ? $_GET['referer_safe_if_absent'] : '';
        $referer_safe_if_first = isset($_GET['referer_safe_if_first']) ? $_GET['referer_safe_if_first'] : false;

        $reason = '';
        $headers = strtolower(get_request_headers());
        if(isAllowed($src, $reason, $filter_by_src, $src_whitelist, $src_blacklist,
            $user_agent, $filter_by_device, $device_whitelist,
            $device_blacklist,
            $country_code, $filter_by_country, $country_whitelist,
            $country_blacklist, $fbclid, $filter_by_fbclid,
            $referer, $filter_by_referer, $referer_whitelist,
            $referer_blacklist, $referer_safe_if_absent,
            $referer_safe_if_first,
            $headers)) {
            echo 'UNSAFE';
        } else {
            echo 'SAFE';
        }
        return true;
    } else {
        return false;
    }
}

function isAllowed($src='', &$reason=null, $filter_by_src=false,
    $src_whitelist='', $src_blacklist='',
    $user_agent='', $filter_by_device=false,
    $device_whitelist='', $device_blacklist='',
    $country_code='', $filter_by_country=false,
    $country_whitelist='', $country_blacklist='',
    $fbclid=null, $filter_by_fbclid=false,
    $referer='', $filter_by_referer=false,
    $referer_whitelist='', $referer_blacklist='',
    $referer_safe_if_absent=false,
    $referer_safe_if_first=false,
    $headers='') {

    $isSrcAllowed = isSrcAllowed(
        $src,
        $filter_by_src,
        $src_whitelist,
        $src_blacklist);

    $isCrawler = isCrawler($user_agent);

    $isDeviceAllowed = isDeviceAllowed(
        $user_agent,
        $filter_by_device,
        $device_whitelist,
        $device_blacklist);

    $isFbClidAllowed = isFbClidAllowed($filter_by_fbclid, $fbclid);

    $isCountryAllowed = isCountryAllowed(
        $country_code,
        $filter_by_country,
        $country_whitelist,
        $country_blacklist);
    
    $isRefererAllowed = isRefererAllowed(
        $referer,
        $filter_by_referer,
        $referer_whitelist,
        $referer_blacklist,
        $referer_safe_if_absent,
        $referer_safe_if_first,
        $headers);
    
    if ($reason !== null) {
        $reason = 'isSrcAllowed:('.$src.','.$isSrcAllowed.') '
            .'isCrawler:('.$isCrawler.') '
            .'isDeviceAllowed:('.$isDeviceAllowed.') '
            .'isFbClidAllowed:('.$isFbClidAllowed.') '
            .'isCountryAllowed:('.$country_code.','.$isCountryAllowed.') '
            .'isRefererAllowed:('.$referer.','.$isRefererAllowed.')';
    }

    verbose(
        '[$isSrcAllowed:'.$isSrcAllowed.']['.
        '$isCrawler:'.$isCrawler.']['.
        '$isDeviceAllowed:'.$isDeviceAllowed.']['.
        '$isFbClidAllowed:'.$isFbClidAllowed.']['.
        '$isCountryAllowed:'.$isCountryAllowed.']['.
        '$isRefererAllowed:'.$isRefererAllowed.']');

    return $isSrcAllowed
        && !$isCrawler
        && $isDeviceAllowed
        && $isFbClidAllowed
        && ($country_code === NULL || $isCountryAllowed)
        && $isRefererAllowed;
}

function isSrcAllowed($src, $mode, $whitelist, $blacklist) {
    /* Returns true if the provided src is allowed to see the unsafe page.
    
    mode can be either "blacklist", "whitelist" or false.

    If false, isSrcAllowed returns true. This means that any src is
    allowed.

    If "whitelist", for srcs listed in $whitelist we return true,
    false otherwise.

    If "blacklist", for srcs listed in $blacklist we return false,
    true otherwise.

    For example:
    $whitelist = "src1,src2"
    $blacklist = "src3"
    */

    $whitelist_array = explode(',', $whitelist);
    $blacklist_array = explode(',', $blacklist);

    if ($mode == "whitelist") {
        return in_array($src, $whitelist_array);
    } elseif ($mode == "blacklist") {
        return !in_array($src, $blacklist_array);
    } elseif ($mode == false) {
        return true;
    } else {
        throw new Exception('The isSrcAllowed mode parameter is invalid. It can be either \'blacklist\', \'whitelist\' or false');
    }
}

function isDeviceAllowed($user_agent, $mode, $whitelist, $blacklist) {
    /* Returns true if the device on the user_agend is allowed to see the
    unsafe page.
    
    mode can be either "blacklist", "whitelist" or false.

    If false, isDeviceAllowed returns true. This means that any country is
    allowed.

    If "whitelist", for devices listed in $whitelist we return true,
    false otherwise.

    If "blacklist", for devices listed in $blacklist we return false,
    true otherwise.

    You can use the values "mobile" and "desktop" for composing the
    whitelist and blacklist.

    For example:
    $whitelist = "mobile,desktop"
    $blacklist = "desktop"
    */

    $whitelist_array = explode(',', $whitelist);
    $blacklist_array = explode(',', $blacklist);

    $user_device = userDevice($user_agent);

    if ($mode == "whitelist") {
        return in_array($user_device, $whitelist_array);
    } elseif ($mode == "blacklist") {
        return !in_array($user_device, $blacklist_array);
    } elseif ($mode == false) {
        return true;
    } else {
        throw new Exception('The isDeviceAllowed mode parameter is invalid. It can be either \'blacklist\', \'whitelist\' or false');
    }
}

function userDevice($user_agent) {
    if (isDesktop($user_agent)) {
        return 'desktop';
    }

    if (isMobile($user_agent)) {
        return 'mobile';
    }

    return 'unknown_device';
}

function isDesktop($user_agent) {
    $desktop_uas = array(
        "Windows",
        "X11",
        "Macintosh",
    );

    for($i = 0; $i < count($desktop_uas); $i++) {
        $desktop_ua = $desktop_uas[$i];

        if (strpos($user_agent, $desktop_ua) !== false) {
            return true;
        }
    }

    return false;
}

function isMobile($user_agent) {
    $mobile_uas = array(
        "Android",
        "iPhone",
        "iPad",
    );

    for($i = 0; $i < count($mobile_uas); $i++) {
        $mobile_ua = $mobile_uas[$i];

        if (strpos($user_agent, $mobile_ua) !== false) {
            return true;
        }
    }

    return false;
}

function isCountryAllowed($country_code, $mode, $whitelist, $blacklist) {
    /* Returns true if the country_code is allowed to see the unsafe page.
    mode can be either "blacklist", "whitelist" or false.

    If false, isCountryAllowed returns true. This means that any country is
    allowed.

    If "whitelist", for country codes listed in $whitelist we return true,
    false otherwise.

    If "blacklist", for country codes listed in $blacklist we return false,
    true otherwise.

    Important: the IP address of the visitor is checked on the geoplugin service
    (http://www.geoplugin.net/json.gp). This might make the page loading a
    little slower, and some rate limits might apply.

    Also, pay attention that you should use the boolean constant false, and not
    the string "false".

    You can find a list of country codes here:
    https://dev.maxmind.com/geoip/legacy/codes/iso3166/

    Some examples of values for whitelist and blacklist:
    ""
    "BR"
    "BR,US"
    "BR,US,AR"
    */
    $whitelist_array = explode(',', $whitelist);
    $blacklist_array = explode(',', $blacklist);

    if ($mode == "whitelist") {
        return in_array($country_code, $whitelist_array);
    } elseif ($mode == "blacklist") {
        return !in_array($country_code, $blacklist_array);
    } elseif ($mode == false) {
        return true;
    } else {
        throw new Exception('The isCountryAllowed mode parameter is invalid. It can be either \'blacklist\', \'whitelist\' or false');
    }
}

function isCrawler($user_agent) {
    // Checks if the user agent is one of Google's crawlers user agents.
    // A list of known google user agents can be found here:
    // https://support.google.com/webmasters/answer/1061943?hl=en
    $google_uas = array(
        "APIs-Google",
        "Mediapartners-Google",
        "AdsBot-Google-Mobile",
        "AdsBot-Google",
        "Googlebot-Image",
        "Googlebot",
        "Googlebot-News",
        "Googlebot-Video",
        "Mediapartners-Google",
        "AdsBot-Google-Mobile-Apps",
        "facebookexternalhit",
        "Yahoo Ad Monitoring"
    );

    for($i = 0; $i < count($google_uas); $i++) {
        $google_ua = $google_uas[$i];
        if (strpos($user_agent, $google_ua) !== false) {
            return $google_ua;
        }    
    }

    return false;
    
}

function isFbClidAllowed($filter_by_fbclid, $fbclid) {
    return $filter_by_fbclid && $fbclid !== null || !$filter_by_fbclid;
}

function isRefererAllowed($referer, $mode, $whitelist, $blacklist,
        $safe_if_absent, $safe_if_first, $headers) {
    /* Returns true if the referer is allowed to see the unsafe page.
    mode can be either "blacklist", "whitelist" or false.

    If false, isRefererAllowed returns true. This means that any referer is
    allowed.

    If "whitelist", for referers listed in $whitelist we return true,
    false otherwise.

    If "blacklist", for referers codes listed in $blacklist we return false,
    true otherwise.

    The strings in the white and black lists are separated by commas. The terms
    don't have to completely match the referer; if the terms are just contained
    in referer it will be considered a match.

    Some examples of values for whitelist and blacklist:
    ""
    "facebook"
    "http://m.facebook.com/,facebook"

    If $safe_if_absent is true, will return false if referer is null.

    If $safe_if_first is true, will return false if the headers start with
    '{"referer"'. We believe Facebook bots headers start with Referer and
    regular users don't.
    */
    verbose('isRefererAllowed params: '.
        '[$referer:'.$referer.']['.
        '$mode:'.$mode.']['.
        '$whitelist:'.$whitelist.']['.
        '$blacklist:'.$blacklist.']['.
        '$safe_if_absent:'.$safe_if_absent.']['.
        '$safe_if_first:'.$safe_if_first.']');

    $whitelist_array = explode(',', $whitelist);
    $blacklist_array = explode(',', $blacklist);

    if ($safe_if_absent) {
        if ($referer === null) {
            return false;
        }
    }

    if ($safe_if_first) {
        if (isRefererFirstOrSecond($headers)) {
            return false;
        }
    }

    if ($mode == "whitelist") {
        foreach ($whitelist_array as $whitelist_term) {
            if (strpos($referer, $whitelist_term) !== false) {
                return true;
            }
        }
        return false;
    } elseif ($mode == "blacklist") {
        foreach ($blacklist_array as $blacklist_term) {
            if (strpos($referer, $blacklist_term) !== false) {
                return false;
            }
        }
        return true;
    } elseif ($mode == false) {
        return true;
    } else {
        throw new Exception('The isRefererAllowed mode parameter is invalid. It can be either \'blacklist\', \'whitelist\' or false');
    }
}

function isRefererSecond($headers) {
    if (preg_match('/^{([^:]+:[^:]+,)"referer"/i', $headers, $matches)) {
        echo $matches[1];
        return true;
    };
    return false;
}

function isRefererFirstOrSecond($headers) {
    return preg_match('/^{([^:]+:[^:]+,)?"Referer"/i', $headers);
}

function startsWith($haystack, $needle)
{
    // https://stackoverflow.com/a/834355/212292
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function get_ip() {
    $ip = $_SERVER["REMOTE_ADDR"];
    $forwarded_for = @explode(',', str_replace(' ', '', $_SERVER['HTTP_X_FORWARDED_FOR']));
    if (filter_var(@$forwarded_for[0], FILTER_VALIDATE_IP))
        $ip = $forwarded_for[0];
    if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    return $ip;
}

function ip_info($ip = NULL, $purpose = "location") {
    // Obtained from: https://stackoverflow.com/a/13600004/212292
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
        $ip = get_ip();
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}

function get_request_headers() {
    $headerList = array();
    foreach ($_SERVER as $name => $value) {
        if (preg_match('/^HTTP_/',$name)) {
            // convert HTTP_HEADER_NAME to Header-Name
            $name = strtr(substr($name,5),'_',' ');
            $name = ucwords(strtolower($name));
            $name = strtr($name,' ','-');
            // add to list
            $headerList[$name] = $value;
        }
    }
    return json_encode($headerList);
}

function get_request() {
    $request = json_encode($_REQUEST);
    return $request;
}

function connect_to_database_server() {
    $servername = "127.0.0.1";
    $username = "root";
    $password = "mysql";
    $servername = "127.0.0.1";
    $username = "cloaker";
    $password = "this is the cloaker password";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $db_name);

    // Check connection
    if ($conn->connect_error) {
        //die("Connection failed: " . $conn->connect_error);
    } 
    //echo "Connected successfully";

    return $conn;
}

function connect_to_database() {
    $db_name = "cloaker";

    $conn = connect_to_database_server();
    $conn->select_db($db_name);

    return $conn;
}

function save_to_database($site_identification=null, $ip=null, $src=null,
    $country_code=null, $user_agent=null, $device=null, $robot=null,
    $page_seen=null, $request=null, $headers=null, $reason=null) {
    $conn = connect_to_database();
    
    $sql = "INSERT INTO logs (site, ip, src, country_code, user_agent, device, robot,
    page_seen, date, request, headers, reason)
    VALUES ('".$site_identification."', '".$ip."', '".$src."', '".$country_code."', '".$user_agent."', '".$device."',
    '".$robot."', '".$page_seen."', NOW(), '".$request."', '".$headers."'
    , '".$reason."')";

    if (mysqli_query($conn, $sql)) {
        //echo "New record created successfully";
    } else {
        //echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

function create_database() {
    $conn = connect_to_database_server();
    // Create database
    $sql = "CREATE DATABASE cloaker";
    if ($conn->query($sql) === TRUE) {
        echo "Database created successfully";
    } else {
        echo "Error creating database: " . $conn->error;
    }

    $conn->close();
    
    $conn = connect_to_database();

    $sql = "CREATE TABLE logs (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
        site VARCHAR(30) NOT NULL,
        ip VARCHAR(30) NOT NULL,
        src VARCHAR(50),
        country_code VARCHAR(5),
        user_agent VARCHAR(200),
        device VARCHAR(50),
        robot VARCHAR(20),
        page_seen VARCHAR(50),
        request VARCHAR(500),
        headers VARCHAR(500),
        reason VARCHAR(500),
        date TIMESTAMP
        )";

    // $sql = "CREATE TABLE sites (
    //     $site_identification
    //     $filter_by_country
    //     $country_whitelist
    //     $country_blacklist
    //     $filter_by_device
    //     $device_whitelist
    //     $device_blacklist
    //     $filter_by_src
    //     $src_whitelist
    //     $src_blacklist
    //     $save_to_database
    //     )";
        
    if ($conn->query($sql) === TRUE) {
        echo "Table logs created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }
}
?>