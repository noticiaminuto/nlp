function up_GetCookie(cookie_name) {
    var name = cookie_name + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function up_SetCookie(cookie_name, cookie_value, expire_days) {
    var d = new Date();
    d.setTime(d.getTime() + (expire_days*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cookie_name + "=" + cookie_value + ";" + expires + ";path=/";
}

function up_CookieIsSet(cookie_name) {
    var cookie_value = GetCookie(cookie_name);
    if (cookie_value != ""){ return true;}
    else{ return false;}

}


function up_WindowLocation(delay,url){
    window.setTimeout(function() {
    window.location.href = url;
    }, delay);
}

function up_SetParams(){
    //Le os urls params da url
    var url_params = location.search.substr(1);

    //Salva as urls params no cookie url_params
    up_SetCookie('up_urlparams', url_params, 180);

    //Encontra todas as ancoras da pagina
    var anchors = document.getElementsByTagName('a');

    //Atualiza o parametro de rastreamento de cada ancora
    for (var i =0; i < anchors.length; i++){
        //pega a ancora
        var anchor = anchors[i];
        //le o link
        var href = anchor.href;

        if (href.charAt(0) != '#'){

            //verifica se o link ja contem parametros de rastreamento
            var parts = href.split('?');

            //Se link tiver parametros de rastreamento, coloca um & para adicionar novos parametros, se nao usa ?
            if (parts.length>1){
                anchor.href = href + '&' + url_params;
            }
            else{
                anchor.href = href + '?' + url_params;
            }
        }

    }
}


function up_SetCheckout(link, redirect_time){
    //Le cookie url_params
    var url_params = up_GetCookie('up_urlparams');

    //verifica se o link ja contem parametros de rastreamento
    var parts = link.split('?');

    //Se link tiver parametros de rastreamento, coloca um & para adicionar novos parametros, se nao usa ?
    if (parts.length>1){
        link = link + '&' + url_params;
    }
    else{
        link = link + '?' + url_params;
    }

    up_WindowLocation(redirect_time,link);
}

function SaveCookiePA(){
    //PARAMETROS DE CLICK ID PROPELLER ADS
    var clickid = GetUrlParam('clickid');
    SetCookie('clickid', clickid, 180);
}

function PostBackPA(){
    var clickid = GetCookie('clickid');
    var server = "http://ad.propellerads.com/conversion.php?aid=76082&pid=&tid=18628&visitor_id="+clickid;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", server, true);
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.send();
}
